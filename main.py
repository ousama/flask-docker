from flask import Flask ,render_template,request
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
nltk.download('vader_lexicon')

app=Flask(__name__)



@app.route('/',methods = ['POST','GET'])
def main():
    if request.method=="POST":
        req=request.form.get('sentiment')
        sid = SentimentIntensityAnalyzer()

        score =sid.polarity_scores(req)

        # 3 attributs neg :negative , pos : positive , neu : neutre
        #print(score)

        if score['neg'] > 0.5 :
            resultat="negative"
            return  render_template('index.html',message=str(resultat))
        elif score['pos']>0.5 :
            resultat="positive"
            return  render_template('index.html',message=str(resultat))
        else :
            resultat = "neutre"
            return render_template('index.html', message=str(resultat))


    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)