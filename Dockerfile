FROM python:3.8-slim-buster
# define the present working directory

WORKDIR /docker-flask-test
# copy the contents into the working dir
Copy . /docker-flask-test

# run pip to install the dependencies of the flask app
RUN pip install -r requirements.txt

ENV FLASK_APP=main.py

ENV FLASK_ENV=development


# define the command to start the container

CMD ["flask", "run", "--host", "0.0.0.0"]



